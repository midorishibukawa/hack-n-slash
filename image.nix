{ pkgs ? import <nixpkgs> { } }:

pkgs.dockerTools.buildLayeredImage {
    name = "ml-dev";
    contents = [ ./result/bin ];
    config.Cmd = [ "/hack_n_slash" ];
}
